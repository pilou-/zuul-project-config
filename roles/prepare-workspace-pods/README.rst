Prepare remote workspaces in Kubernetes pods

This role can be used instead of the :zuul:role:`prepare-workspace`
role when the synchronize module doesn't work with kubectl connection.
It copies the prepared source repos to the pods' cwd using the `kubectl
cp` command.

This role is intended to run once before any other role in a Zuul job.

**Role Variables**

.. zuul:rolevar:: kubernetes_pods
   :default: {{ zuul.resources }}

   The dictionary of pod name, pod information to copy the sources to.
